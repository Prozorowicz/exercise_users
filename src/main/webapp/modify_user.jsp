<%@ page import="java.util.ArrayList" %>
<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="com.javagda14.exercise.GENDER" %>
<%@ page import="java.time.LocalDateTime" %><%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 25/09/2018
  Time: 20:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Modify User</title>
</head>
<body>
<%
    List<User> users;
    if (session.getAttribute("user_list") != null) {
        users = (List<User>) session.getAttribute("user_list");

    } else {
        users = new ArrayList<>();
    }
    String user_id = request.getParameter("modified_id");
    User searched = null;

    if (user_id != null) {
        long modifiedId = Long.parseLong(user_id);
        Iterator<User> it = users.iterator();
        while (it.hasNext()) {
            User user = it.next();
            if (user.getId() == modifiedId) {
                searched = user;
                it.remove();
                break;
            }
        }
    }
    searched.setFirstName(request.getParameter("firstName"));
    searched.setLastName(request.getParameter("lastName"));
    searched.setAddress(request.getParameter("address"));
    searched.setUserName(request.getParameter("userName"));
    LocalDate birthdate = LocalDate.parse(request.getParameter("birthDate"),DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    searched.setBirthDate(birthdate);
    GENDER gender =GENDER.valueOf(request.getParameter("gender"));
    searched.setGender(gender);
    int height=Integer.parseInt(request.getParameter("height"));
    searched.setHeight(height);
    users.add(searched);
    session.setAttribute("user_list",users);
    response.sendRedirect("user_list.jsp");

%>
</body>
</html>

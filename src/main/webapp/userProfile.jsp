<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="com.javagda14.exercise.GENDER" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="java.time.LocalDate" %><%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 24/09/2018
  Time: 20:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Profil Uzytkownika</title>
</head>
<body>
<% long licznik = 0; %>
<%
    User u = new User();
    u.setId(licznik++);
    u.setFirstName(request.getParameter("firstName"));
    u.setLastName(request.getParameter("lastName"));
    u.setAddress(request.getParameter("address"));
    u.setUserName(request.getParameter("userName"));
    LocalDate birthdate = LocalDate.parse(request.getParameter("birthDate"), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    u.setBirthDate(birthdate);
    GENDER gender = GENDER.valueOf(request.getParameter("gender"));
    u.setGender(gender);
    int height = Integer.parseInt(request.getParameter("height"));
    u.setHeight(height);
    u.setJoinDate(LocalDateTime.now());
    boolean isOk = true;
    if (!Character.isUpperCase(u.getFirstName().charAt(0))) {
        isOk = false;
    }
    if (!Character.isUpperCase(u.getLastName().charAt(0))) {
        isOk = false;
    }
    if (!Character.isUpperCase(u.getLastName().charAt(0))) {
        isOk = false;
    }
    if (!Character.isUpperCase(u.getLastName().charAt(0))) {
        isOk = false;
    }
    if (!Character.isUpperCase(u.getLastName().charAt(0))) {
        isOk = false;
    }

%>

<div>
    <table>
        <thead>
        <th>Nazwa parametru</th>
        <th>Wartosc</th>
        </thead>
        <tr>
            <td>Id:</td>
            <td><%= u.getId()%>
            </td>
        </tr>
        <tr>
            <td>Username:</td>
            <td><%= u.getUserName()%>
            </td>
        </tr>
        <tr>
            <td>FirstName:</td>
            <td><%= u.getFirstName()%>
            </td>
        </tr>
        <tr>
            <td>Lastname:</td>
            <td><%= u.getLastName()%>
            </td>
        </tr>
        <tr>
            <td>address:</td>
            <td><%= u.getAddress()%>
            </td>
        </tr>
        <tr>
            <td>Height:</td>
            <td><%= u.getHeight()%>
            </td>
        </tr>
        <tr>
            <td>Joindate:</td>
            <td><%= u.getJoinDate()%>
            </td>
        </tr>
        <tr>
            <td>Gender:</td>
            <td><%= u.getGender()%>
            </td>
        </tr>
    </table>
</div>
</body>
</html>

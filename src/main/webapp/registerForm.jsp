<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.javagda14.exercise.GENDER" %><%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 24/09/2018
  Time: 20:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Formularz</title>
</head>
<body>
<%
    List<User> users;
    if (session.getAttribute("user_list") != null) {
        users = (List<User>) session.getAttribute("user_list");

    } else {
        users = new ArrayList<>();
    }
    String user_id = request.getParameter("user_id");


    User searched = null;
    boolean female = true;
    if (user_id != null) {
        long modifiedId = Long.parseLong(user_id);

        Iterator<User> it = users.iterator();
        while (it.hasNext()) {
            User user = it.next();
            if (user.getId() == modifiedId) {
                searched = user;
                break;
            }
        }
        if (searched != null) {
            female = searched.getGender() == GENDER.FEMALE;
        }
    }
    //User u = new User();
%>
<form action="<%= searched==null?"register_user.jsp":"modify_user.jsp"%>" method="get">
    <input type="hidden" hidden value="<%=searched==null?"":searched.getId()%>" name="modified_id">
    <h2>Form:</h2>
    <div>
        <label for="firstName">Name:</label>
        <input id="firstName" name="firstName" type="text" value="<%=searched == null?"":searched.getFirstName()%>">
    </div>
    <%--private String firstName;--%>
    <%--private String lastName;--%>
    <%--private String userName;--%>
    <%--private LocalDateTime birthDate;--%>
    <%--private LocalDateTime joinDate;--%>
    <%--private String address;--%>
    <%--private int height;--%>
    <%--private GENDER gender;--%>
    <div>
        <label for="lastName">LastName:</label>
        <input id="lastName" name="lastName" type="text" value="<%=searched == null?"":searched.getLastName()%>">
    </div>
    <div>
        <label for="userName">userName:</label>
        <input id="userName" name="userName" type="text" value="<%=searched == null?"":searched.getUserName()%>">
    </div>
    <div>
        <label for="birthDate">birthDate:</label>
        <input id="birthDate" name="birthDate" type="date" value="<%=searched == null?"":searched.getBirthDate().toString()%>">
    </div>
    <div>
        <label for="address">address:</label>
        <input id="address" name="address" type="text" value="<%=searched == null?"":searched.getAddress()%>">
    </div>
    <div>
        <label for="height">height:</label>
        <input id="height" name="height" type="number" min="60" max="230"
               value="<%=searched == null?"":searched.getHeight()%>">
    </div>
    <div>
        <label>Gender:</label>
        <input name="gender" type="radio" value="MALE" <%= female?"":"checked"%>>MALE </input>
        <input name="gender" type="radio" value="FEMALE" <%= female?"checked":""%>>FEMALE </input>
    </div>
    <div>
        <input type="submit" value="przeslij">
    </div>

</form>
</body>
</html>

<%@ page import="java.util.ArrayList" %>
<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %><%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 25/09/2018
  Time: 19:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Remove User</title>
</head>
<body>
<%
    List<User> users;
    if(session.getAttribute("user_list")!=null){
        users = (List<User>) session.getAttribute("user_list");

    }else {
        users = new ArrayList<>();
    }
    String removeuserId = request.getParameter("user_id");
    Long removedId = Long.parseLong(removeuserId);
    Iterator<User> it = users.iterator();
    while (it.hasNext()){
        User user = it.next();
        if (user.getId()==removedId){
            it.remove();
            break;
        }
    }
    session.setAttribute("user_list",users);
    response.sendRedirect("user_list.jsp");
%>
</body>
</html>

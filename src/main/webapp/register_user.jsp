<%@ page import="com.javagda14.exercise.User" %>
<%@ page import="java.time.LocalDate" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="com.javagda14.exercise.GENDER" %>
<%@ page import="java.time.LocalDateTime" %>
<%@ page import="java.util.List" %>
<%@ page import="javax.jws.soap.SOAPBinding" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Tomasz
  Date: 25/09/2018
  Time: 18:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Action Register User</title>
</head>
<body>
<%! long licznik = 0; %>
<%
    User u  =new User();
    u.setId(licznik++);
    u.setFirstName(request.getParameter("firstName"));
    u.setLastName(request.getParameter("lastName"));
    u.setAddress(request.getParameter("address"));
    u.setUserName(request.getParameter("userName"));
    LocalDate birthdate = LocalDate.parse(request.getParameter("birthDate"),DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    u.setBirthDate(birthdate);
    GENDER gender =GENDER.valueOf(request.getParameter("gender"));
    u.setGender(gender);
    int height=Integer.parseInt(request.getParameter("height"));
    u.setHeight(height);
    u.setJoinDate(LocalDateTime.now());
    //////////////////////////////////////
    List<User> users;
    if(session.getAttribute("user_list")!=null){
    users = (List<User>) session.getAttribute("user_list");

    }else {
        users = new ArrayList<>();
    }
    users.add(u);
session.setAttribute("user_list",users);
response.sendRedirect("user_list.jsp");
%>
</body>
</html>

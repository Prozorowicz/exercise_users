package com.javagda14.exercise;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class User {
    private long id;
    private String firstName;
    private String lastName;
    private String userName;
    private LocalDate birthDate;
    private LocalDateTime joinDate;
    private String address;
    private int height;
    private GENDER gender;

    public User() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }


    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDateTime getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(LocalDateTime joinDate) {
        this.joinDate = joinDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public GENDER getGender() {
        return gender;
    }

    public void setGender(GENDER gender) {
        this.gender = gender;
    }

    public String toSerializedLine() {
        StringBuilder builder = new StringBuilder();
        builder.append(id).append(";");
        builder.append(firstName).append(";");
        builder.append(lastName).append(";");
        builder.append(userName).append(";");
        builder.append(DateTimeFormatter.ofPattern("yyyy-MM-dd").format(birthDate)).append(";");
        builder.append(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm").format(joinDate)).append(";");
        builder.append(address).append(";");
        builder.append(height).append(";");
        builder.append(gender).append(";");
        return builder.toString();
    }

    public void loadFromSerializedLine(String line) {
        String[] values = line.split(";");
        setId(Long.parseLong(values[0]));
        setFirstName(values[1]);
        setLastName(values[2]);
        setUserName(values[3]);
        setBirthDate(LocalDate.parse(values[4], DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        setJoinDate(LocalDateTime.parse(values[5], DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));
        setAddress(values[6]);
        setHeight(Integer.parseInt(values[7]));
        setGender(GENDER.valueOf(values[8]));
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userName='" + userName + '\'' +
                ", birthDate=" + birthDate +
                ", joinDate=" + joinDate +
                ", address='" + address + '\'' +
                ", height=" + height +
                ", gender=" + gender +
                '}';
    }
}
